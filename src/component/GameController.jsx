import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import './GameController.css';
const base64 = require('base-64');

export default class CameraInfoPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: true,
            gamepad: null,
            yPressed: false,
            xPressed: false,
            bPressed: false,
            aPressed: false,
            upPressed: false,
            downPressed: false,
            leftPressed: false,
            rightPressed: false,
            homePressed: false,
            upSent: false,
            downSent: false,
            leftSent: false,
            rightSent: false,
            aSent: false,
            bSent: false,
            xSent: false,
            ySent: false,
            homeSent: false
        }
    }

    render() {
        return (
            <Container>
                <div className="xbox-container">
                    <div className="main">
                        <div className="dpad">
                            <label className="dpad-label">Static Pan</label>
                            <div className="circle"></div>
                            <div className={this.state.upPressed ? 'dpad-up pressed' : 'dpad-up'} ></div>
                            <div className={this.state.downPressed ? 'dpad-down pressed' : 'dpad-down'}></div>
                            <div className={this.state.leftPressed ? 'dpad-left pressed' : 'dpad-left'}></div>
                            <div className={this.state.rightPressed ? 'dpad-right pressed' : 'dpad-right'}></div>
                        </div>
                        <div className="buttons">
                            <label className="button-label buttony-label">Zoom In</label>
                            <div className={this.state.yPressed ? 'buttony pressed' : 'buttony'}>Y</div>
                            <label className="button-label buttonx-label">Focus In</label>
                            <div className={this.state.xPressed ? 'buttonx pressed' : 'buttonx'}>X</div>
                            <label className="button-label buttona-label">Zoom Out</label>
                            <div className={this.state.aPressed ? 'buttona pressed' : 'buttona'}>A</div>
                            <label className="button-label buttonb-label">Focus Out</label>
                            <div className={this.state.bPressed ? 'buttonb pressed' : 'buttonb'}>B</div>
                        </div>
                    </div>
                </div>
            </Container>
        )
    }

    getCameraInformation() {
        let sessionStorage = window.sessionStorage;
        let cameraInformation = sessionStorage.getItem('cameraInfomation');
        return JSON.parse(cameraInformation)
    }

    updateStatus() {
        let gamepad = navigator.getGamepads()[0],
            buttons = gamepad.buttons,
            aPressed = buttons[0].pressed,
            bPressed = buttons[1].pressed,
            xPressed = buttons[2].pressed,
            yPressed = buttons[3].pressed,
            upPressed = buttons[12].pressed,
            downPressed = buttons[13].pressed,
            leftPressed = buttons[14].pressed,
            rightPressed = buttons[15].pressed,
            homePressed = buttons[8].pressed;

        this.setState({
            aPressed,
            bPressed,
            xPressed,
            yPressed,
            upPressed,
            downPressed,
            leftPressed,
            rightPressed,
            homePressed
        });

        // Zooming out
        if (yPressed && !this.state.ySent) {
            this.controlCamera('zoomin', false);
            this.setState({
                ySent: true
            });
        } else if (!yPressed && this.state.ySent) {
            this.controlCamera('zoomstop', true)
            this.setState({
                ySent: false
            });
        }

        // Zooming out
        if (aPressed && !this.state.aSent) {
            this.controlCamera('zoomout', false);
            this.setState({
                aSent: true
            });
        } else if (!aPressed && this.state.aSent) {
            this.controlCamera('zoomstop', true)
            this.setState({
                aSent: false
            });
        }

        // Focus in
        if (xPressed && !this.state.xSent) {
            this.controlCamera('focusin', false);
            this.setState({
                xSent: true
            });
        } else if (!xPressed && this.state.xSent) {
            this.controlCamera('focusstop', true)
            this.setState({
                xSent: false
            });
        }

        // Focus out
        if (bPressed && !this.state.bSent) {
            this.controlCamera('focusout', false);
            this.setState({
                bSent: true
            });
        } else if (!bPressed && this.state.bSent) {
            this.controlCamera('focusstop', true)
            this.setState({
                bSent: false
            });
        }

        // Pan up
        if (upPressed && !this.state.upSent) {
            this.controlCamera('up', false);
            this.setState({
                upSent: true
            });
        } else if (!upPressed && this.state.upSent) {
            this.controlCamera('ptzstop', true)
            this.setState({
                upSent: false
            });
        }

        // Pan down
        if (downPressed && !this.state.downSent) {
            this.controlCamera('down', false);
            this.setState({
                downSent: true
            });
        } else if (!downPressed && this.state.downSent) {
            this.controlCamera('ptzstop', true)
            this.setState({
                downSent: false
            });
        }

        // Pan right
        if (rightPressed && !this.state.rightSent) {
            this.controlCamera('right', false);
            this.setState({
                rightSent: true
            });
        } else if (!rightPressed && this.state.rightSent) {
            this.controlCamera('ptzstop', true)
            this.setState({
                rightSent: false
            });
        }

        // Pan left
        if (leftPressed && !this.state.leftSent) {
            this.controlCamera('left', false);
            this.setState({
                leftSent: true
            });
        } else if (!leftPressed && this.state.leftSent) {
            this.controlCamera('ptzstop', true)
            this.setState({
                leftSent: false
            });
        }

        // Go home
        if (homePressed && !this.state.homeSent) {
            this.controlCamera('home', false);
            this.setState({
                homeSent: false
            });
        } else {
            this.setState({
                homeSent: false
            });
        }
    }

    controlCamera(ptzcmd, isStop) {
        let cameraInfo = this.getCameraInformation();
        let url = 'http://' + cameraInfo.ipAddress + '/cgi-bin/ptzctrl.cgi?ptzcmd&' + ptzcmd;
        if (!isStop) {
            url += '&' + cameraInfo.staticPanSpeed
        }

        let headers = new Headers();
        headers.append('Authorization', 'Basic ' + base64.encode(cameraInfo.username + ":" + cameraInfo.password));
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        fetch(url, {
            method: 'GET',
            headers: headers
        }).then(res => console.log(res));
    }

    componentDidMount() {
        var me = this;
        window.addEventListener('gamepadconnected', function (e) {
            me.setState({
                isConnected: true,
                gamepad: e.gamepad
            });
            setInterval(me.updateStatus.bind(me), 50);
        });
    }
}