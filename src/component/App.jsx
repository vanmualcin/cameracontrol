import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import CameraInfoPanel from './CameraInfoPanel';
import GameController from './GameController';
import './App.css';
function App() {
    return (
        <Container>
            <Row>
                <Col className="header-container">
                    <h1>Van's PTZ Camera Control - Demo</h1>
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={6}>
                    <GameController />
                </Col>
                <Col sm={12} md={6}>
                    <CameraInfoPanel />
                </Col>
            </Row>
        </Container>
    );
}

export default App;