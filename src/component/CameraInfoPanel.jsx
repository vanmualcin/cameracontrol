import React from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

export default class CameraInfoPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ipAddress: '192.168.1.107',
            // viscaPort: '554',
            username: 'admin',
            password: 'admin', // default factory password
            staticPanSpeed: '5'
        };
        this.persistState();
        this.onIpAddressChange = this.onIpAddressChange.bind(this);
        // this.onViscaPortChange = this.onViscaPortChange.bind(this);
        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onStaticSpeedChange = this.onStaticSpeedChange.bind(this);
    }

    persistState() {
        let sessionStorage = window.sessionStorage;
        sessionStorage.setItem('cameraInfomation', JSON.stringify(this.state));
    }

    onIpAddressChange(event) {
        this.setState({
            ipAddress: event.target.value
        });
        this.persistState();
    }

    // onViscaPortChange(event) {
    //     this.setState({
    //         viscaPort: event.target.value
    //     });
    //     this.persistState();
    // }

    onUsernameChange(event) {
        this.setState({
            username: event.target.value
        });
        this.persistState();
    }

    onPasswordChange(event) {
        this.setState({
            password: event.target.value
        });
        this.persistState();
    }

    onStaticSpeedChange(event) {
        this.setState({
            staticPanSpeed: event.target.value
        });
        this.persistState();
    }

    render() {
        return (
            <Container>
                <Form>
                    <Form.Group>
                        <Form.Label>IP Address</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="IP Address"
                            defaultValue={this.state.ipAddress}
                            onChange={this.onIpAddressChange}
                        />
                    </Form.Group>
                </Form>
                {/* <Form>
                    <Form.Group>
                        <Form.Label>Visca Port</Form.Label>
                        <Form.Control
                            required
                            type="text"
                            placeholder="VISCA Port"
                            defaultValue={this.state.viscaPort}
                            onChange={this.onViscaPortChange}
                        />
                    </Form.Group>
                </Form> */}
                <Form>
                    <Form.Group>
                        <Form.Label>Username</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Username"
                            defaultValue={this.state.username}
                            onChange={this.onUsernameChange}
                        />
                    </Form.Group>
                </Form>
                <Form>
                    <Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Passw0rd"
                            defaultValue={this.state.password}
                            onChange={this.onPasswordChange}
                        />
                    </Form.Group>
                </Form>
                <Form>
                    <Form.Group>
                        <Form.Label>Static Pan Speed</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="5"
                            defaultValue={this.state.staticPanSpeed}
                            onChange={this.onStaticPanSpeedChange}
                        />
                    </Form.Group>
                </Form>
            </Container>
        );
    }
}